# ***********************************************
# ***          Loco363 - Parts - T6           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


from enum import Enum
import math

import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class T6(generic.bases.View):
	def __init__(self, c = None, name = None, *args, label = None, label_lines = 3, font_size = 12, npos = (0, 28), **kwargs):
		'''Constructor
		
		@param c - center Coords
		@param name - placed part label
		@param label - whether to draw a label with description
			True/False = for direct setting
			None = draw it when there is a name specified
		@param label_lines - number of allowed (and verticaly aligned) label text lines
		@param font_size - font size in pt of label text
		@param npos - part label text position
		'''
		
		super().__init__(c, *args, **kwargs)
		c = self.getCoords()
		
		# optional label
		if label or (label == None and name != None and name != ''):
			# label body
			self.add(gen_draw.shapes.Rectangle(
				C(c, (-(45-0.8)/2, -(22.5-0.4))),
				45 - 0.8,# 45mm outer width
				56 - 0.8,# 56mm outer height
				properties={
					'fill': 'none',
					'stroke': 'black',
					'stroke-width': '0.8mm'
				}
			))
		
		# optional name
		if name != None and name != '':
			# split multi-line label
			lines = name.split('\n')
			name = ''
			if len(lines) > label_lines:
				raise ValueError('Given more label text lines than allowed!')
			
			# if there is no special spacing, put the first line directly into the main text field
			first = (label_lines - len(lines))/2
			if first <= 0:
				name = lines[0]
				lines = lines[1:]
			
			# text shape
			t = gen_draw.shapes.Text(
				C(c, npos),
				name,
				properties={
					'text-anchor': 'middle',
					'fill': 'black',
					'style': 'font-size:'+str(font_size)+'pt; font-weight:bold;'
				}
			)
			
			# add first line when spacing required
			if first > 0:
				t.addLine(lines[0], str(first * font_size) + 'pt')
				lines = lines[1:]
			
			# rest of the lines
			for l in lines:
				t.addLine(l, str(font_size) + 'pt')
			
			# add the text shape into the drawing
			self.add(t)
		
		# metal ring
		self.add(self._createRing())
	# constructor
	
	def _createRing(self):
		'''Creates drawing of metal ring
		
		@return ring drawing
		'''
		
		c = self.getCoords()
		ring = gen_draw.Drawing(c)
		
		ring.add(gen_draw.shapes.Circle(
			c,
			17.3 - 0.3,# 34.6mm outer diameter
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': '0.6mm'
			}
		))
		
		ring.add(gen_draw.shapes.Circle(
			c,
			16,
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 0.5
			}
		))
		
		ring.add(gen_draw.shapes.Circle(
			c,
			12.8 + 0.2,# 25.6mm inner diameter
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': '0.4mm'
			}
		))
		
		return ring
	# _createRing
# class T6


class T6x(T6):
	'''Ancestor of T6 parts with top'''
	
	def __init__(self, c = None, name = None, color = None, *args, **kwargs):
		'''Constructor
		
		@param c - center Coords
		@param name - placed part label
		@param color - None or color of top
		'''
		
		super().__init__(c, name, *args, **kwargs)
		c = self.getCoords()
		
		# optional color
		self._setTopColor(color)
		
		# top - just prepare, not add into itself (as PushButton with Mushroom cap needs own version)
		self._top = self._createTop()
	# constructor
	
	def _setTopColor(self, color):
		'''Sets top's color
		
		@param color - color code or None for default
		'''
		
		self._top_fill_color = color
		self._top_stroke_color = color if color else 'black'
	# _setTopColor
	
	def _createTop(self, dashed = False):
		'''Creates drawing of top's base (inner circle & fill)
		
		@param dashed - draw the inner circle with dashed stroke
		@return top drawing
		'''
		
		c = self.getCoords()
		top = gen_draw.Drawing(c)
		
		# inner circle semi-transparent fill
		if self._top_fill_color:
			top.add(gen_draw.shapes.Circle(
				c,
				12.5,
				properties={
					'fill': self._top_fill_color,
					'opacity': 0.3
				}
			))
		
		# inner circle
		props = {
			'fill': 'none',
			'stroke': self._top_stroke_color,
			'stroke-width': '0.3mm'
		}
		
		if dashed:
			props['stroke-dasharray'] = '1,1'
		
		top.add(gen_draw.shapes.Circle(
			c,
			12.5 - 0.15,# 25mm outer diameter
			properties=props
		))
		
		return top
	# _createTop
# class T6x


class T6_PilotLight(T6x):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		
		# top
		self.add(self._top)
		
		# pilot-light symbol
		c = self.getCoords()
		symbol = gen_draw.Drawing(c)
		self._top.add(symbol)
		
		r1 = 4
		r2 = 10
		for i in range(0, 360, 45):
			i = math.radians(22.5 + i)# workaround as range cannot take floats
			symbol.add(gen_draw.shapes.Line(
				C(c, (r1*math.sin(i), r1*math.cos(i))),
				C(c, (r2*math.sin(i), r2*math.cos(i))),
				properties={
					'stroke': self._top_stroke_color,
					'stroke-linecap': 'round',
					'stroke-width': '0.5mm'
				}
			))
	# constructor
# class T6_PilotLight


class T6_PushButton_Cap(Enum):
	'''Defines available caps for push-button'''
	
	Flat = 0
	Ergo = 1
	Mushroom = 2
# T6_PushButton_Cap

class T6_PushButton(T6x):
	def __init__(self, *args, cap = T6_PushButton_Cap.Flat, **kwargs):
		'''Constructor
		
		@param cap - type of button cap to depict
		'''
		
		super().__init__(*args, **kwargs)
		
		# top
		if cap == T6_PushButton_Cap.Mushroom:
			self._top = self._createTop(True)
		self.add(self._top)
		
		# button cap
		if cap == T6_PushButton_Cap.Mushroom:
			self._top.add(self._createCapMushroom())
		else:
			# tightening notches
			self._top.add(self._createNotches())
			
			# additional circles for ergo cap
			if cap == T6_PushButton_Cap.Ergo:
				self._top.add(self._createCapErgo())
	# constructor
	
	def _createNotches(self):
		'''Creates drawing of cap tightening notches
		
		@return notches drawing
		'''
		
		c = self.getCoords()
		d = gen_draw.Drawing(c)
		
		for i in (-1, 1):
			# left + right
			d.add(gen_draw.shapes.Polygon(
				c,
				[
					C(c, (i*12.3,  1)),
					C(c, (i*11.5,  1)),
					C(c, (i*11.5, -1)),
					C(c, (i*12.3, -1))
				],
				properties={
					'fill': 'none',
					'stroke': self._top_stroke_color,
					'stroke-linecap': 'round',
					'stroke-linejoin': 'round',
					'stroke-width': 0.5
				}
			))
			
			# top + bottom
			d.add(gen_draw.shapes.Polygon(
				c,
				[
					C(c, ( 1, i*12.3)),
					C(c, ( 1, i*11.5)),
					C(c, (-1, i*11.5)),
					C(c, (-1, i*12.3))
				],
				properties={
					'fill': 'none',
					'stroke': self._top_stroke_color,
					'stroke-linecap': 'round',
					'stroke-linejoin': 'round',
					'stroke-width': 0.5
				}
			))
		
		return d
	# _createNotches
	
	def _createCapErgo(self):
		'''Creates drawing of ergo-cap circles
		
		@return cap drawing
		'''
		
		c = self.getCoords()
		d = gen_draw.Drawing(c)
		
		for r in range(4, 10, 2):
			d.add(gen_draw.shapes.Circle(
				c,
				r,
				properties={
					'fill': 'none',
					'stroke': self._top_stroke_color,
					'stroke-dasharray': '1,1',
					'stroke-width': 0.5
				}
			))
		
		return d
	# _createCapErgo
	
	def _createCapMushroom(self):
		'''Creates drawing of mushroom-cap
		
		@return cap drawing
		'''
		
		c = self.getCoords()
		d = gen_draw.Drawing(c)
		
		# mushroom semi-transparent fill
		if self._top_fill_color:
			d.add(gen_draw.shapes.Circle(
				c,
				24,
				properties={
					'fill': self._top_fill_color,
					'opacity': 0.3
				}
			))
		
		# mushroom diameter
		d.add(gen_draw.shapes.Circle(
			c,
			24 - 0.3,# 48mm outer diameter
			properties={
				'fill': 'none',
				'stroke': self._top_stroke_color,
				'stroke-dasharray': '1,1',
				'stroke-width': '0.6mm'
			}
		))
		
		return d
	# _createCapMushroom
# class T6_PushButton


class T6_SwitchRot(T6x):
	'''Rotational switches - base class'''
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		
		# top
		self.add(self._top)
	# constructor
	
	def _addHandle(self, angle, alt = False):
		'''Creates drawing of handle & adds it into drawing of top
		
		@param angle - angle of the handle
		@param alt - if True, draw the handle as alternative switch position (dashed)
		@return handle drawing
		'''
		
		c = self.getCoords()
		c = C(c, -angle)# we are using negative angle for counter-clockwise, but the Coords transformations uses right-hand-rule angles
		
		handle = gen_draw.Drawing(c)
		
		# compute radius for 8mm thick handle
		r = 12.5 - 0.15# 25mm outer diameter
		r = math.sqrt(r**2 - 4**2)
		
		# handle semi-transparent
		if self._top_fill_color:
			# as there is problem with polygons in SVG compiler (and these are usualy rendered via group of lines), it cannot be handled by filled polygon
			handle.add(gen_draw.shapes.Line(
				C(c, (0,  r)),
				C(c, (0, -r)),
				properties={
					'opacity': 0.3 if alt else 0.5,
					'stroke': self._top_fill_color,
					'stroke-width': '8mm'
				}
			))
		
		props = {
			'stroke': self._top_stroke_color,
			'stroke-width': '0.2mm' if alt else '0.3mm'
		}
		
		if alt:
			props['stroke-dasharray'] = '1,1'
		
		handle.add(gen_draw.shapes.Polygon(
			c,
			[
				C(c, (-4,  r)),
				C(c, (-4, -r)),
				C(c, ( 4, -r)),
				C(c, ( 4,  r))
			],
			properties=props
		))
		
		self._top.add(handle)
		return handle
	# _addHandle
# class T6_SwitchRot


class T6_SwitchRot2Pos(T6_SwitchRot):
	def __init__(self, *args, pl = None, **kwargs):
		'''Constructor
		
		@param pl - switch positions labels [left, right]
		'''
		
		super().__init__(*args, **kwargs)
		
		# switch handle
		self._addHandle(-45)
		self._addHandle( 45, True)
		
		# position labels
		if pl:
			c = self.getCoords()
			d = gen_draw.Drawing(c)
			self.add(d)
			
			# left
			d.add(gen_draw.shapes.Text(
				C(c, (-8, 16)),
				pl[0],
				properties={
					'text-anchor': 'end',
					'fill': 'black',
					'style': 'font-size:12pt; font-weight:bold;'
				}
			))
			
			# right
			d.add(gen_draw.shapes.Text(
				C(c, (8, 16)),
				pl[1],
				properties={
					'text-anchor': 'start',
					'fill': 'black',
					'style': 'font-size:12pt; font-weight:bold;'
				}
			))
	# constructor
# class T6_SwitchRot2Pos


class T6_SwitchRot3Pos(T6_SwitchRot):
	def __init__(self, *args, pl = None, label_lines = 2, **kwargs):
		'''Constructor
		
		@param pl - switch positions labels [left, center, right]
		'''
		
		super().__init__(*args, label_lines=label_lines, **kwargs)
		
		# switch handle
		self._addHandle(-45, True)
		self._addHandle(  0)
		self._addHandle( 45, True)
		
		# position labels
		if pl:
			c = self.getCoords()
			d = gen_draw.Drawing(c)
			self.add(d)
			
			# left
			d.add(gen_draw.shapes.Text(
				C(c, (-13, 12)),
				pl[0],
				properties={
					'text-anchor': 'end',
					'fill': 'black',
					'style': 'font-size:12pt; font-weight:bold;'
				}
			))
			
			# center
			d.add(gen_draw.shapes.Text(
				C(c, (0, 18)),
				pl[1],
				properties={
					'text-anchor': 'middle',
					'fill': 'black',
					'style': 'font-size:12pt; font-weight:bold;'
				}
			))
			
			# right
			d.add(gen_draw.shapes.Text(
				C(c, (13, 12)),
				pl[2],
				properties={
					'text-anchor': 'start',
					'fill': 'black',
					'style': 'font-size:12pt; font-weight:bold;'
				}
			))
	# constructor
# class T6_SwitchRot3Pos


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(T6(name='T6'), True))
