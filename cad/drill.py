# ***********************************************
# ***          Loco363 - Parts - T6           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class T6(generic.bases.Drill):
	def __init__(self, *args, label = True, **kwargs):
		'''Constructor
		
		@param label - whether to draw hole for label's backlight
		'''
		
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# optional label
		if label:
			# label body
			self.add(gen_draw.shapes.Rectangle(
				C(c, (-45/2, -22.5)),
				45,
				56,
				properties={
					'fill': self.COLOR_UNI
				}
			))
			
			self.add(gen_draw.shapes.Circle(
				c,
				17,
				properties={
					'fill': 'white'
				}
			))
			
			# backlight hole
			self.add(generic.DrillHole(C(c, (0, 25.5)), 6, cpos=(0, -7, 'middle')))
		
		# main hole
		self.add(generic.DrillHole(c, 30, cpos=(0, -5, 'middle')))
	# constructor
# class T6


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(T6(), True))
