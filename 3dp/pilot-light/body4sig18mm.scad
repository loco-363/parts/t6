/**
***********************************************
***          Loco363 - Parts - T6           ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <../body.scad>;


module T6_PilotLight_Body4sig18mm(){
	difference(){
		T6_Body(25);
		
		translate([0, 0, -1]){
			intersection(){
				cylinder(d=16.4, h=27);
				
				translate([-(15.5/2), -9, -1])
					cube([15.5, 18, 27]);
			}
		}
		
		translate([0, 0, -0.1])
			cylinder(d1=25, d2=20, h=7+0.1);
		
		translate([0, 0, 5])
			cylinder(d=23, h=2);
		
		translate([0, 0, 12])
			cylinder(d=22, h=14);
	}
	
	// bridge support
	translate([0, 0, 12])
		cylinder(d=25, h=0.3);
} // T6_PilotLight_Body4sig18mm


T6_PilotLight_Body4sig18mm($fn=200);
