/**
***********************************************
***          Loco363 - Parts - T6           ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <../generic/ir.scad>;

use <../latches.scad>;


module T6_PushButton_MicroSwitch_Brick(){
	// constants
	BH = 14.5;// brick height
	BW = 20;// brick width
	SW = 7;// switch width
	
	
	difference(){
		translate([-BW/2, -25/2, 0])
			cube([BW, 32, BH]);
		
		// latches for connection with body
		translate([0, 0, BH])
			T6_Latches();
		
		// micro-switch cut-out
		translate([-SW/2, -6.5, -1])
			cube([SW, 21, 1+BH+1]);
		
		translate([-SW/2, -11, BH-6])
			cube([SW, 5, 6+1]);
		
		// micro-switch mounting
		for(i = [0 : 1])
			translate([-11, -6.5+0.5+5.2+i*9.5, BH-4.8-7])
				rotate([0, 90, 0])
					cylinder(d=2.5, h=1+BW+1);
		
		// signal LED
		for(i = [-1 : 2 : 1])
			translate([i*5, 3, -1])
				cylinder(d=1.2, h=1+BH+1);
		
		for(i = [-1 : 2 : 1])
			translate([i*5, 3, 0])
				rotate([0, i*90, 0])
					cylinder(d=1.2, h=BW/2-5+1);
		
		translate([-5, -1.2/2+3, BH-1.5])
			cube([2*5, 1.2, 3]);
	}
} // T6_PushButton_MicroSwitch_Brick


T6_PushButton_MicroSwitch_Brick($fn=20);
