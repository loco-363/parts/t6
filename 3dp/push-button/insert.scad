/**
***********************************************
***          Loco363 - Parts - T6           ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


module T6_PushButton_Insert(){
	difference(){
		union(){
			// base
			cylinder(d=12.6, h=17);
			
			// mounting collar
			translate([0, 0, 10])
				cylinder(d=13.6, h=7);
			
			translate([0, 0, 15])
				cylinder(d1=13.6, d2=18, h=2);
		}
		
		// central hole
		translate([0, 0, -1])
			cylinder(d=9, h=19);
	}
	
	rotate([180, 0, 0]){
		difference(){
			cylinder(d=12.6, h=7);
			
			translate([0, 0, -1]){
				translate([-1, 0, 0])
					cylinder(d=5, h=9);
				
				translate([-2, -7, 0])
					cube([14, 14, 9]);
				
				translate([-7-4, 2.5, 0])
					cube([7, 7, 9]);
				
				translate([-7-4, -7-2.5, 0])
					cube([7, 7, 9]);
				
				// bevelled edges of tip
				translate([0, 0, 9])
					rotate([0, 45, 0])
						translate([-2.5, -10, -2.5])
							cube([5, 20, 5]);
				translate([-8, 0, 9])
					rotate([0, 45, 0])
						translate([-2.5, -10, -2.5])
							cube([5, 20, 5]);
				translate([-6, -5-2.5, 9])
					rotate([0, 45, 0])
						translate([-2.5, -5, -2.5])
							cube([5, 10, 5]);
				translate([-6, 5+2.5, 9])
					rotate([0, 45, 0])
						translate([-2.5, -5, -2.5])
							cube([5, 10, 5]);
			}
		}
		
		translate([0, 0, -5]){
			difference(){
				cylinder(d=12.6, h=5);
				
				translate([-2, -7, -1])
					cube([14, 14, 7]);
				
				translate([-1, 0, 0])
					rotate([0, 45, 0])
						translate([-2.5, -7, -2.5])
							cube([5, 14, 5]);
			}
		}
	}
} // T6_PushButton_Insert


T6_PushButton_Insert($fn=100);
