/**
***********************************************
***          Loco363 - Parts - T6           ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <../../generic/threads.scad>;


module T6_PushButton_Caps_Mushroom(){
	// cap
	scale([1, 1, 20/48])
		sphere(d=48);
	
	// stem
	cylinder(d=25, h=10+20);
	
	// thread
	cylinder(d=20.5, h=10+20+3.5);
	
	translate([0, 0, 10+20])
		metric_thread(diameter=21.7, length=3);
	
	// pin
	translate([0, 0, 10+20])
		cylinder(d=7.4, h=3.5+10);
} // T6_PushButton_Caps_Mushroom


module T6_PushButton_Caps_Mushroom_print(){
	difference(){
		union(){
			T6_PushButton_Caps_Mushroom();
			
			translate([60, 0, 0])
				rotate([180, 0, 0])
					T6_PushButton_Caps_Mushroom();
		}
		
		translate([-50, -50, -100])
			cube([200, 100, 100]);
	}
} // T6_PushButton_Caps_Mushroom_print


T6_PushButton_Caps_Mushroom($fn=200);
