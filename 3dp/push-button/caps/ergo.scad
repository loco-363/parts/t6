/**
***********************************************
***          Loco363 - Parts - T6           ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <../../generic/threads.scad>;


module T6_PushButton_Caps_Ergo(){
	difference(){
		union(){
			cylinder(d=20.5, h=6);
			cylinder(d=25, h=2.5);
			
			translate([0, 0, 2.5])
				metric_thread(diameter=21.7, length=3);
			
			// bevel
			translate([0, 0, -0.5])
				cylinder(d2=25, d1=23, h=0.5);
		}
		
		// notches for tightening
		for(i = [-1 : 2 : 1]){
			translate([-1 + i*(11.5+1), -1, -1])
				cube(2);
		}
		for(i = [-1 : 2 : 1]){
			translate([-1, -1 + i*(11.5+1), -1])
				cube(2);
		}
		
		// ergonomic dip
		translate([0, 0, -50+1-0.5])
			sphere(d=100);
	}
} // T6_PushButton_Caps_Ergo


module T6_PushButton_Caps_Ergo_print(){
	difference(){
		union(){
			translate([0, 0, -1.5])
				T6_PushButton_Caps_Ergo();
			
			translate([30, 0, 0])
				rotate([180, 0, 0])
					translate([0, 0, -1.5])
						T6_PushButton_Caps_Ergo();
		}
		
		translate([-25, -25, -10])
			cube([100, 50, 10]);
	}
} // T6_PushButton_Caps_Ergo_print


T6_PushButton_Caps_Ergo($fn=200);
