/**
***********************************************
***          Loco363 - Parts - T6           ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <../../generic/threads.scad>;


module T6_PushButton_Caps_Flat(){
	difference(){
		union(){
			cylinder(d=20.5, h=6);
			cylinder(d=25, h=2.5);
			
			translate([0, 0, 2.5])
				metric_thread(diameter=21.7, length=3);
		}
		
		// notches for tightening
		for(i = [-1 : 2 : 1]){
			translate([-1 + i*(11.5+1), -1, -1])
				cube(2);
		}
		for(i = [-1 : 2 : 1]){
			translate([-1, -1 + i*(11.5+1), -1])
				cube(2);
		}
	}
} // T6_PushButton_Caps_Flat


module T6_PushButton_Caps_Flat_print(){
	difference(){
		union(){
			translate([0, 0, -1.5])
				T6_PushButton_Caps_Flat();
			
			translate([30, 0, 0])
				rotate([180, 0, 0])
					translate([0, 0, -1.5])
						T6_PushButton_Caps_Flat();
		}
		
		translate([-25, -25, -10])
			cube([100, 50, 10]);
	}
} // T6_PushButton_Caps_Flat_print


T6_PushButton_Caps_Flat($fn=200);
