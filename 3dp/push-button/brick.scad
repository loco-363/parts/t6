/**
***********************************************
***          Loco363 - Parts - T6           ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <../generic/ir.scad>;

use <../latches.scad>;


module T6_PushButton_Brick(){
	difference(){
		translate([-10, -12.5, 0])
			cube([20, 25, 14]);
		
		// latches for connection with housing
		translate([0, 0, 14])
			T6_Latches();
		
		// interior cut-out
		translate([-7, -10, -1])
			cube([14, 20, 14+2]);
		
		// cut-outs for optional cover
		for(i = [-1 : 1]){
			translate([-9, -1+i*9, 12])
				cube([18, 2, 3]);
		}
	}
	
	difference(){
		union(){
			// IR-LED
			translate([0, 0, 5])
				rotate([0, 0, 90])
					ir_flat();
			
			// IR-PHOTO
			translate([0, 9.5, 5])
				rotate([0, 0, -90])
					ir_flat();
			
			// opto-parts fixes
			translate([0, -1.5, 5]){
				translate([3, 0, 0])
					cube([4.5, 3, 7]);
				translate([-4.5-3, 0, 0])
					cube([4.5, 3, 7]);
			}
			
			translate([0, 4.5, 5]){
				translate([3, 0, 0])
					cube([4.5, 6, 7]);
				translate([-4.5-3, 0, 0])
					cube([4.5, 6, 7]);
			}
		}
		
		// bevelled edges of opto-barrier "chamber"
		translate([0, 3, 13.5])
			rotate([45, 0, 0])
				translate([-7, -2.5, -2.5])
					cube([14, 5, 5]);
		translate([0, 6, 12.7])
			rotate([0, 45, 0])
				translate([-3, -2, -3])
					cube([6, 4, 6]);
	}
	
	// signal LED
	difference(){
		translate([-7, -1.5-3, 5])
			cube([14, 3.5, 7]);
		
		for(i = [-1 : 2 : 1]){
			translate([i*1.27, -5.5, 7.5])
				rotate([-35, 0, 0])
					cylinder(d=1.2, h=7);
		}
	}
	
	// bottom filling
	difference(){
		translate([-7.5, -10.5, 0])
			cube([15, 21, 5]);
		
		translate([0, 0, -1])
			rotate([0, 0, 90])
				ir_pins(7);
		
		translate([0, 9.5, -1])
			rotate([0, 0, -90])
				ir_pins(7);
		
		// holes for signal LED
		for(i = [-1 : 2 : 1]){
			translate([i*1.27, -9, -1])
				cylinder(d=1.2, h=7);
		}
	}
} // T6_PushButton_Brick


module T6_PushButton_Brick_ext(){
	difference(){
		T6_PushButton_Brick();
		
		for(i = [-1 : 2 : 1]){
			translate([-10-1, i*4.5, 2.5])
				rotate([0, 90, 0])
					cylinder(d=1.2, h=22);
			translate([-10, i*4.5, -0.5])
				cylinder(d=1.2, h=3);
			translate([10, i*4.5, -0.5])
				cylinder(d=1.2, h=3);
		}
	}
} // T6_PushButton_Brick_ext


T6_PushButton_Brick($fn=20);
