/**
***********************************************
***          Loco363 - Parts - T6           ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <generic/threads.scad>;


module T6_Body(h){
	if(h >= 7){
		// shape (connection of parts for rendering)
		cylinder(d=25, h=h);
		
		// upper thread (metal ring)
		cylinder(d=28.5, h=5);
		
		translate([0, 0, 1])
			metric_thread(diameter=29.7, pitch=1.1, length=5-1+0.5);
		
		//
		translate([0, 0, 5])
			cylinder(d=30, h=2);
		
		translate([14.5, 0, 5])
			cylinder(d=4, h=2);
		
		// lower thread (plastic nut - for mounting through panel)
		translate([0, 0, 7])
			cylinder(d=27.8, h=h-7);
		
		translate([0, 0, 6.5])
			metric_thread(diameter=29.9, pitch=1.5, length=h-7-1+0.5);
	}
} // T6_Body


T6_Body(h=30, $fn=200);
